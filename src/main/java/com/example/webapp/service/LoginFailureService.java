package com.example.webapp.service;

import com.example.webapp.dao.LoginFailureRepository;
import com.example.webapp.entity.LoginFailure;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
public class LoginFailureService {

    private final LoginFailureRepository loginFailureRepository;

    public LoginFailureService(LoginFailureRepository loginFailureRepository) {
        this.loginFailureRepository = loginFailureRepository;
    }

    @Transactional
    public void clearLoginFailures(UUID accountId) {
        loginFailureRepository.deleteAllByAccountId(accountId);
    }

    public void createLoginFailure(UUID accountId) {
        loginFailureRepository.save(new LoginFailure(accountId, new Date()));
    }

    public int countLoginFailureAttempt(UUID accountId, Date date) {
        return loginFailureRepository.countByAccountIdAndDateAfter(accountId, date);
    }
}
