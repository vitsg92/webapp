package com.example.webapp.service;

import com.example.webapp.auth.JwtProvider;
import com.example.webapp.entity.Account;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
public class AuthenticationService {

    private static final long LOGIN_ATTEMPT_PERIOD_IN_MS = 300000;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenService jwtTokenService;
    private final LoginFailureService loginFailureService;
    private final JwtProvider jwtProvider;
    @Value("${jwt.token.validity}")
    private long jwtTokenValidity;

    public AuthenticationService(AccountService accountService,
                                 PasswordEncoder passwordEncoder,
                                 JwtTokenService jwtTokenService,
                                 LoginFailureService loginFailureService,
                                 JwtProvider jwtProvider) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenService = jwtTokenService;
        this.loginFailureService = loginFailureService;
        this.jwtProvider = jwtProvider;
    }

    public String login(String login, String password) {
        Account account = accountService.findByLogin(login);
        Objects.requireNonNull(account, "Неправильные логин или пароль");
        checkMaxLoginAttempt(account.getId());
        if (!passwordEncoder.matches(password, account.getPassword())) {
            loginFailureService.createLoginFailure(account.getId());
            throw new RuntimeException("Неправильные логин или пароль");
        }
        loginFailureService.clearLoginFailures(account.getId());
        return createToken(account);
    }

    public void logout(String hashToken) {
        jwtTokenService.deleteJwtToken(hashToken);
    }

    private void checkMaxLoginAttempt(UUID accountId) {
        Date date = new Date(new Date().getTime() - LOGIN_ATTEMPT_PERIOD_IN_MS);
        if (loginFailureService.countLoginFailureAttempt(accountId, date) > 5) {
            throw new RuntimeException("Превышено максимальное количество попыток авторизации. " +
                    "Попробуйте повторить через несколько минут");
        }
    }

    private String createToken(Account account) {
        Date expiring = new Date(new Date().getTime() + jwtTokenValidity);
        String hashToken = jwtProvider.generateToken(account.getLogin(), expiring);
        jwtTokenService.saveJwtToken(hashToken, expiring);
        return hashToken;
    }

}
