package com.example.webapp.service;

import com.example.webapp.dao.JwtTokenRepository;
import com.example.webapp.entity.JwtToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class JwtTokenService {

    private final JwtTokenRepository jwtTokenRepository;

    public JwtTokenService(JwtTokenRepository jwtTokenRepository) {
        this.jwtTokenRepository = jwtTokenRepository;
    }

    public JwtToken saveJwtToken(String hash, Date expiring) {
        JwtToken jwtToken = new JwtToken(hash, expiring);
        return jwtTokenRepository.save(jwtToken);
    }

    @Transactional
    public void deleteJwtToken(String hashToken) {
        jwtTokenRepository.deleteByHash(hashToken);
    }

    public boolean tokenIsExists(String hashToken) {
        return jwtTokenRepository.countByHash(hashToken) > 0;
    }

}
