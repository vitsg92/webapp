package com.example.webapp.service;

import com.example.webapp.dao.AccountRepository;
import com.example.webapp.entity.Account;
import com.example.webapp.entity.Payment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Service
public class AccountService {

    private static final BigDecimal PAYMENT_AMOUNT = BigDecimal.valueOf(1.1);
    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account findByLogin(String login) {
        return accountRepository.findByLogin(login);
    }

    @Transactional
    public Account payment(UUID accountId) {
        Account account = accountRepository.findAccountForUpdate(accountId);
        BigDecimal balance = account.getBalance();
        if (balance.compareTo(PAYMENT_AMOUNT) < 0) {
            throw new RuntimeException("Платеж отклонен. Сумма на счету менее " + PAYMENT_AMOUNT + "USD");
        }
        account.setBalance(balance.subtract(PAYMENT_AMOUNT));
        account.getPayments().add(new Payment(account.getId(), BigDecimal.valueOf(1.1), new Date()));
        return accountRepository.save(account);
    }
}
