package com.example.webapp.controller;

import com.example.webapp.auth.CustomUserDetails;
import com.example.webapp.service.AccountService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.UUID;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    private final AccountService accountService;

    public PaymentController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public BigDecimal payment() {
        UUID accountId = ((CustomUserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal()).getAccountId();
        return accountService.payment(accountId).getBalance();
    }
}
