package com.example.webapp.controller;

import com.example.webapp.dto.AuthenticationRequest;
import com.example.webapp.service.AuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.example.webapp.auth.JwtFilter.getTokenFromRequest;

@RestController
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public String login(@RequestBody AuthenticationRequest request) {
        return authenticationService.login(request.getLogin(), request.getPassword());
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request) {
        authenticationService.logout(getTokenFromRequest(request));
    }
}
