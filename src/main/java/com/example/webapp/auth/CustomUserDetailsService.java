package com.example.webapp.auth;

import com.example.webapp.entity.Account;
import com.example.webapp.service.AccountService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private final AccountService accountService;

    public CustomUserDetailsService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public CustomUserDetails loadUserByUsername(String username) {
        Account account = accountService.findByLogin(username);
        if (account == null) {
            throw new UsernameNotFoundException("User with username: " + username + " not found");
        }
        return new CustomUserDetails(account);
    }
}
