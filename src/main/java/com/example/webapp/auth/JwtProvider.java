package com.example.webapp.auth;

import com.example.webapp.service.JwtTokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    private final JwtTokenService jwtTokenService;
    @Value("${jwt.token.secret}")
    private String jwtTokenSecret;

    public JwtProvider(JwtTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }

    public String generateToken(String login, Date expiring) {
        return Jwts.builder()
                .setSubject(login)
                .setExpiration(expiring)
                .signWith(SignatureAlgorithm.HS256, jwtTokenSecret)
                .compact();
    }

    public void validateToken(String token) {
        if (token == null || token.isEmpty()) {
            throw new RuntimeException("JWT token not found");
        }
        Jws<Claims> claims = Jwts.parser().setSigningKey(jwtTokenSecret).parseClaimsJws(token);
        if (claims.getBody().getExpiration().before(new Date()) || !jwtTokenService.tokenIsExists(token)) {
            throw new RuntimeException("JWT token is expired or invalid");
        }
    }

    public String getLoginFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtTokenSecret).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }
}
