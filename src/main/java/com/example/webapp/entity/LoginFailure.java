package com.example.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.UUID;

@Entity
public class LoginFailure extends BaseEntity {

    @Column(name = "account_id")
    private UUID accountId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public LoginFailure() {
    }

    public LoginFailure(UUID accountId, Date date) {
        this.accountId = accountId;
        this.date = date;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
