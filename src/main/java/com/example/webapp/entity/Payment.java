package com.example.webapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
public class Payment extends BaseEntity {

    @Column(name = "account_id")
    private UUID accountId;
    private BigDecimal amount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Payment() {
    }

    public Payment(UUID accountId, BigDecimal amount, Date date) {
        this.accountId = accountId;
        this.amount = amount;
        this.date = date;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
