package com.example.webapp.entity;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
public class JwtToken extends BaseEntity {

    private String hash;
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiring;

    public JwtToken() {
    }

    public JwtToken(String hash, Date expiring) {
        this.hash = hash;
        this.expiring = expiring;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Date getExpiring() {
        return expiring;
    }

    public void setExpiring(Date expiring) {
        this.expiring = expiring;
    }
}
