package com.example.webapp.dao;

import com.example.webapp.entity.LoginFailure;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.UUID;

public interface LoginFailureRepository extends JpaRepository<LoginFailure, UUID> {

    void deleteAllByAccountId(UUID accountId);

    int countByAccountIdAndDateAfter(UUID accountId, Date date);
}
