package com.example.webapp.dao;

import com.example.webapp.entity.JwtToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JwtTokenRepository extends JpaRepository<JwtToken, UUID> {

    void deleteByHash(String hash);

    int countByHash(String hash);
}
