package com.example.webapp.dao;

import com.example.webapp.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {

    Account findByLogin(String login);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select a from Account a where a.id = :id")
    Account findAccountForUpdate(UUID id);
}
